﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using IniFileParser;

namespace FirstTask
{
    class Program
    {

        private const string readPath = @"C:\Users\Kolya\Downloads\Пример_исходного_файла.ini";

        private const string writePath = @"C:\Users\Kolya\Desktop\Пример.ini";

        static void Main(string[] args)
        {

            Dictionary<string, List<string>> dictionaryOfIniTables = new Dictionary<string, List<string>>();

            List<string> sections = new List<string>();


            string text = string.Empty;
            try
            {
                text = File.ReadAllText(readPath);
            }
            catch (Exception e) { Console.WriteLine(e.Message); }

            dictionaryOfIniTables = iniParser.Parse(text);

            SortIni(dictionaryOfIniTables, sections);
            WriteIniFile(dictionaryOfIniTables, sections);

            Console.ReadKey();
        }



        private static void WriteIniFile(Dictionary<string, List<string>> dictionaryOfIniTables, List<string> list)
        {

            try
            {
                using (StreamWriter sw = new StreamWriter(writePath, false, System.Text.Encoding.Default))
                {

                    foreach (var key in list)
                    {
                        sw.WriteLine("{0}", "\n" + "[" + key + "]");

                        foreach (var item in dictionaryOfIniTables[key])
                        {
                            sw.WriteLine(item);
                        }
                    }

                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static void SortIni(Dictionary<string, List<string>> dictionaryOfIniTables, List<string> sections)
        {
            foreach (var item in dictionaryOfIniTables)
            {
                sections.Add(item.Key);
                item.Value.Sort();
            }
            sections.Sort();
        }


    }
}
