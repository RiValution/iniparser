﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace IniFileParser
{
    public class iniParser
    {

        public static Dictionary<string, List<string>> Parse(string text)
        {
            Dictionary<string, List<string>> tables = new Dictionary<string, List<string>>();

            List<string> currentSection = new List<string>();

            var lines = text.Split(new[] { "\n" }, StringSplitOptions.RemoveEmptyEntries)
                                   .Where(t => !string.IsNullOrWhiteSpace(t))
                                   .Select(t => t.Trim());
            foreach (var line in lines)
            {


                if (line.StartsWith("[") && line.EndsWith("]"))
                {
                    currentSection = new List<string>();
                    tables[line.Substring(1, line.LastIndexOf("]") - 1)] = currentSection;
                }
                else if (line.Contains("="))
                {
                    currentSection.Add(line);
                }
                else
                {
                    Console.WriteLine("Wrong format");
                    break;
                }
            }
            return tables;


        }


    }
}
